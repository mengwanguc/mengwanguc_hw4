#include <iostream>

using namespace std;

class B {
public:
	B() {}
	virtual void f() {}
};

class D : public B {
public:
	D() {}
};


int main() {
	B b;
	B& rb = b;
	try {
		dynamic_cast<D&>(rb);
	}
	catch(bad_cast){
		cout << "bad cast" << endl;
	}

	return 0;
}

