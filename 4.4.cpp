#include <iostream>
#include <memory>
using namespace std;

struct node
{
	int key_value;
	shared_ptr<node> left;
	shared_ptr<node> right;
	node(int key) {
		key_value = key;
		left = NULL;
		right = NULL;
	}
};

class btree
{
public:
	btree();

	void insert(int key);
	shared_ptr<node> search(int key);

private:
	void insert(int key, shared_ptr<node> &leaf);
	shared_ptr<node> search(int key, shared_ptr<node> &leaf);

	shared_ptr<node> root;
};

btree::btree()
{
	root = NULL;
}

void btree::insert(int key, shared_ptr<node> &leaf)
{
	if (key< leaf->key_value)
	{
		if (leaf->left != NULL)
			insert(key, leaf->left);
		else
		{
			leaf->left = make_shared<node>(key);
		}
	}
	else if (key >= leaf->key_value)
	{
		if (leaf->right != NULL)
			insert(key, leaf->right);
		else
		{
			leaf->right = make_shared<node>(key);
		}
	}
}

shared_ptr<node> btree::search(int key, shared_ptr<node> &leaf)
{
	if (leaf != NULL)
	{
		if (key == leaf->key_value)
			return leaf;
		if (key<leaf->key_value)
			return search(key, leaf->left);
		else
			return search(key, leaf->right);
	}
	else return NULL;
}


void btree::insert(int key)
{
	if (root != NULL)
		insert(key, root);
	else
	{
		root = make_shared<node>(key);
	}
}

shared_ptr<node> btree::search(int key)
{
	return search(key, root);
}


void main() {
	// initialization
	btree bt;
	for (int i = 0; i < 10; i++) {
		bt.insert(i);
	}

	// test
	auto res1 = bt.search(8);
	if (res1 == NULL) {
		cout << "8 not found" << endl;
	}
	else {
		cout << "find " << res1->key_value << endl;
	}

	auto res2 = bt.search(20);
	if (res2 == NULL) {
		cout << "20 not found" << endl;
	}
	else {
		cout << "find " << res2->key_value << endl;
	}
}