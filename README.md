### What is this repository for? ###

This is my solutions to homework 4 in the course C++.


### hw 4.1 ###

The code is not correct. If the program catches error and shuts down in g(), then cout won't be flushed.
4.1.cpp is the source code of my correction to the original code.

Extra credit:
It's possible that f() will complete, when the program gets overflow or something else in g() and shuts down.
 


### hw 4.2 ###

If dynamic_cast casts an invalid reference, a bad_cast exception would be thrown.

4.2.cpp is the source code of my example.


### hw 4.4 ###

4.4.cpp is my source code.

Yes, I think my modifications are an improvement, since we don't need to worry about the destruction of the tree. In the original code, we need to think of some algorithms to destruct the tree and avoid memory leak. But now there is no need to worry.