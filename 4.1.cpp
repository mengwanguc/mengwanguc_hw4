#include <iostream>

using namespace std;

void g() {
	
}

int h() { return 0; }


int f() {
	cout << "Some text";
	try {
		g(); // g and h are functions whose definitions are unknown
	}
	catch (...) {
		cout.flush();
		return 0;
	}
	try {
		cout << h();
	}
	catch (...) {
		cout.flush();
		return 0;
	}
	cout.flush();
	return 0;
}


int main() {
	int x = f();
	cout << x << endl;
}